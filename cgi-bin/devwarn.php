#!/usr/bin/php
<?php
$stdin = fopen('php://stdin', 'r');

$payload = <<<EOT
<div id="developementWarningMessage">This is a development site. for testing purposes only!</div>
<style>
#developementWarningMessage
{
        color: #fff;
        position: fixed;
        top: 0;
        left:0;
        width: 100%;
        height:40px;
        line-height: 40px;
        font-family: sans-serif;
        font-size: 20px;
        text-align: center;
        z-index: 99999;
        text-transform: uppercase;
        background: #a90329;
        background: -moz-linear-gradient(top,  #a90329 0%, #8f0222 44%, #6d0019 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a90329), color-stop(44%,#8f0222), color-stop(100%,#6d0019));
        background: -webkit-linear-gradient(top,  #a90329 0%,#8f0222 44%,#6d0019 100%);
        background: -o-linear-gradient(top,  #a90329 0%,#8f0222 44%,#6d0019 100%);
        background: -ms-linear-gradient(top,  #a90329 0%,#8f0222 44%,#6d0019 100%);
        background: linear-gradient(to bottom,  #a90329 0%,#8f0222 44%,#6d0019 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a90329', endColorstr='#6d0019',GradientType=0 );
}
</style>
<script type="text/javascript">
(function(){
        window.top['dev-message'] = "pending";
        var body = document.getElementsByTagName("body")[0];

        body.addEventListener("load", init(), false);

        function init() {
                document.getElementsByTagName("html")[0].style.marginTop = "40px";
        }
        window.top['dev-message'] = "inserted";
})();
</script>

EOT;

while($line = fgets($stdin)){
  $line = preg_replace( '/<\/body>/', $payload . '</body>', $line );
  print $line;
  if(isset($stdout)) fwrite($stdout, $line);
}
