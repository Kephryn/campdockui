#!/usr/bin/env node

var fs = require('fs');
var cgi = require('./cgi');
var renderHamlFile = require('/usr/lib/node_modules/hamljs').renderFile;


var server = cgi.createServer(function(request, response) {
	response.writeHead(200, {'Content-Type': 'text/html'});

	try {
		response.end(renderHamlFile(process.env.PATH_TRANSLATED));
	}
	catch(err) {
		response.end(JSON.stringify(err));
	}
});

server.listen();