#!/usr/bin/env node

var cgi = require('./cgi');
var sass = require('/usr/lib/node_modules/node-sass');

var server = cgi.createServer(function(request, response) {
	response.writeHead(200, {'Content-Type': 'text/css'});

	var result = sass.renderSync({
		file: process.env.PATH_TRANSLATED
	});
	response.write(result.css);
	response.end();
});

server.listen();