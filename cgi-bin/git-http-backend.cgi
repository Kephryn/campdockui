#!/bin/bash
HTTP_HOST="${HTTP_HOST/.dock0/}"
VHOST="/var/www/html"

copy=${HTTP_HOST}
len=${#copy}
for((i=$len-1;i>=0;i--)); do rev="$rev${copy:$i:1}"; done
HTTP_HOST=${rev}

arr=$(echo $HTTP_HOST | tr "." "\n")

for x in $arr; do
	rev=""
	copy=${x}
	len=${#copy}
	for((i=$len-1;i>=0;i--)); do rev="$rev${copy:$i:1}"; done
	VHOST="$VHOST/$rev"
done

VHOST="$VHOST"


# echo "Content-type: text/html; charset=iso-8859-1\n\n";
# echo $VHOST
# echo "\n"
# echo "/www/cafott/site/public_html/"
# echo "\n"

# # PATH_INFO=$SCRIPT_URL
export GIT_PROJECT_ROOT=$VHOST
# # REMOTE_USER=$REDIRECT_REMOTE_USER
export GIT_HTTP_EXPORT_ALL=true

/usr/lib/git-core/git-http-backend
