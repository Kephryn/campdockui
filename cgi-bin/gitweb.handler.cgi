#!/bin/bash
HTTP_HOST="${HTTP_HOST/.lan/}"
# HTTP_HOST="${HTTP_HOST/.yvon2.dev/}"
VHOST=""

copy=${HTTP_HOST}
len=${#copy}
for((i=$len-1;i>=0;i--)); do rev="$rev${copy:$i:1}"; done
HTTP_HOST=${rev}

arr=$(echo $HTTP_HOST | tr "." "\n")

for x in $arr; do
	rev=""
	copy=${x}
	len=${#copy}
	for((i=$len-1;i>=0;i--)); do rev="$rev${copy:$i:1}"; done
	VHOST="$VHOST/$rev"
done

VHOST="$VHOST/public_html"

export GITWEB_CONFIG="gitweb_config.perl"
export GATEWAY_INTERFACE="CGI/1.1"
export HTTP_ACCEPT="*/*"
export REQUEST_METHOD="GET"
export QUERY_STRING="a=project_index&p=$VHOST"

perl -- /var/www/cgi-bin/gitweb.cgi
