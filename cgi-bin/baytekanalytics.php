#!/usr/bin/php
<?php
$stdin = fopen('php://stdin', 'r');

$payload = <<<EOT
<script src="http://baytek-analytics.staging.bayteksystems.com/ba.js"></script>
EOT;

while($line = fgets($stdin)){
  $line = preg_replace( '/<\/body>/', $payload . '</body>', $line );
  print $line;
  if(isset($stdout)) fwrite($stdout, $line);
}