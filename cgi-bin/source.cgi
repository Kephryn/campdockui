#!/usr/bin/env node

var fs = require('fs');
var cgi = require('./cgi');
var nsh = require('/usr/lib/node_modules/node-syntaxhighlighter');

var server = cgi.createServer(function(request, response) {
	response.writeHead(200, {'Content-Type': 'text/html'});

	response.end(process.env.PATH_TRANSLATED);


	fs.readFile(process.env.PATH_TRANSLATED.replace(/\.source$/, ''), function (err, data) {
		if (err){
			response.write(err);
			response.end();
		}

		var parts = process.env.PATH_TRANSLATED.replace(/\.source$/, '').split('.');

		response.write(process.env.PATH_TRANSLATED.replace(/\.source$/, ''));
		response.end();
		// # return respond.end(parts[parts.length - 1]);
	});
});

server.listen();