$GIT = "/usr/bin/git";
$projectroot = "/var/www/html/intrinsec";
$project_maxdepth = 1;
$home_link_str = "Camp Dock GitWeb";
$projects_list_group_categories = 1;
$export_ok = "GITWEB_EXPORT_OK";
$feature{'avatar'}{'default'} = ['gravatar'];
$feature{'highlight'}{'default'} = [1];
