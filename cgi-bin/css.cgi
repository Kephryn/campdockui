#!/bin/sh

# disable filename globbing
set -f

echo "Content-type: text/css; charset=iso-8859-1"
echo

cat $PATH_TRANSLATED | postcss --use autoprefixer