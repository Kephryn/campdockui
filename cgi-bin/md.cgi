#!/usr/bin/env node

var fs = require('fs');
var cgi = require('./cgi');
var marked = require('/usr/lib/node_modules/marked');

var server = cgi.createServer(function(request, response) {
	response.writeHead(200, {'Content-Type': 'text/html'});

	fs.readFile(process.env.PATH_TRANSLATED, function (err, data) {
		if (err){
			response.write(err);
			response.end();
		}

		marked(data.toString(), function(err, md){
			if (err){
				response.write(err);
				response.end();
			}

			response.write(md);
			response.end();
		});
	});
});

server.listen();