<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template');
});

Route::group(['prefix' => 'database'], function(){
	Route::get('adminer', function () {
	    return view('welcome');
	});

	Route::get('phpmyadmin', function () {
	    return view('welcome');
	});

	Route::get('phpredisadmin', function () {
	    return view('welcome');
	});
});

Route::group(['prefix' => 'apache'], function(){
	Route::get('dashboard', 'Apache@dashboard');
	Route::get('cgi', function () {
	    return view('template');
	});
});
