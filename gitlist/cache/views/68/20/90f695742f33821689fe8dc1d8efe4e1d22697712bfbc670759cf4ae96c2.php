<?php

/* branch_menu.twig */
class __TwigTemplate_682090f695742f33821689fe8dc1d8efe4e1d22697712bfbc670759cf4ae96c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"btn-group pull-left space-right\" id=\"branchList\">
    <button class=\"btn dropdown-toggle\" data-toggle=\"dropdown\">browsing: <strong>";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["branch"]) ? $context["branch"] : null), "html", null, true);
        echo "</strong> <span class=\"caret\"></span></button>

    <div class=\"dropdown-menu\">
        <div class=\"search\">
            <input class=\"search\" placeholder=\"Filter branch/tags\" autofocus>
        </div>
    <ul class=\"unstyled list\">
    <li class=\"dropdown-header\">Branches</li>
    ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["branches"]) ? $context["branches"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("branch", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["item"]) ? $context["item"] : null))), "html", null, true);
            echo "\"><span class=\"item\">";
            echo twig_escape_filter($this->env, (isset($context["item"]) ? $context["item"] : null), "html", null, true);
            echo "</span></a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "    ";
        if ((isset($context["tags"]) ? $context["tags"] : null)) {
            // line 14
            echo "    <li class=\"dropdown-header\">Tags</li>
    ";
            // line 15
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["tags"]) ? $context["tags"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 16
                echo "        <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("branch", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["item"]) ? $context["item"] : null))), "html", null, true);
                echo "\"><span class=\"item\">";
                echo twig_escape_filter($this->env, (isset($context["item"]) ? $context["item"] : null), "html", null, true);
                echo "</span></a></li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "    ";
        }
        // line 19
        echo "    </ul>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "branch_menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 19,  69 => 18,  54 => 15,  51 => 14,  33 => 10,  22 => 2,  19 => 1,  106 => 27,  101 => 30,  96 => 28,  89 => 24,  87 => 23,  84 => 22,  81 => 21,  78 => 20,  76 => 19,  58 => 16,  52 => 11,  41 => 9,  35 => 5,  32 => 4,  29 => 3,  220 => 15,  213 => 13,  207 => 12,  203 => 10,  200 => 9,  153 => 69,  147 => 66,  142 => 64,  138 => 62,  135 => 61,  131 => 59,  126 => 56,  114 => 53,  110 => 52,  105 => 51,  102 => 49,  99 => 29,  97 => 46,  94 => 27,  91 => 44,  86 => 43,  79 => 38,  73 => 18,  67 => 15,  65 => 33,  61 => 31,  59 => 30,  48 => 13,  46 => 20,  43 => 10,  40 => 8,  37 => 11,  31 => 5,  26 => 3,);
    }
}
