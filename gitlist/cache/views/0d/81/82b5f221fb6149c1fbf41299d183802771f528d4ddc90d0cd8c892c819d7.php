<?php

/* commits_list.twig */
class __TwigTemplate_0d8182b5f221fb6149c1fbf41299d183802771f528d4ddc90d0cd8c892c819d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["commits"]) ? $context["commits"] : null)) {
            // line 2
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["commits"]) ? $context["commits"] : null));
            foreach ($context['_seq'] as $context["date"] => $context["commit"]) {
                // line 3
                echo "<table class=\"table table-striped table-bordered\">
    <thead>
        <tr>
            <th colspan=\"3\">";
                // line 6
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : null), "F j, Y"), "html", null, true);
                echo "</th>
        </tr>
    </thead>
    <tbody>
        ";
                // line 10
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["commit"]) ? $context["commit"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 11
                    echo "        <tr>
            <td width=\"5%\"><img src=\"https://gravatar.com/avatar/";
                    // line 12
                    echo twig_escape_filter($this->env, md5(twig_lower_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "author"), "email"))), "html", null, true);
                    echo "?s=40\" /></td>
            <td width=\"95%\">
                <span class=\"pull-right\"><a class=\"btn btn-small\" href=\"";
                    // line 14
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commit", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commit" => $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "hash"))), "html", null, true);
                    echo "\"><i class=\"icon-list-alt\"></i> View ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "shortHash"), "html", null, true);
                    echo "</a></span>
                <h4>";
                    // line 15
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "message"), "html", null, true);
                    echo "</h4>
                <span><a href=\"mailto:";
                    // line 16
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "author"), "email"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "author"), "name"), "html", null, true);
                    echo "</a> authored on ";
                    echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('format_date')->getCallable(), array($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "date"))), "html", null, true);
                    echo "</span>
            </td>
        </tr>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 20
                echo "    </tbody>
</table>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['date'], $context['commit'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 24
            echo "<p>No results found.</p>
";
        }
        // line 26
        echo "
";
        // line 27
        if (((isset($context["page"]) ? $context["page"] : null) != "searchcommits")) {
            // line 28
            echo "<ul class=\"pager\">
    ";
            // line 29
            if (($this->getAttribute((isset($context["pager"]) ? $context["pager"] : null), "current") != 0)) {
                // line 30
                echo "    <li class=\"previous\">
        <a href=\"?page=";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pager"]) ? $context["pager"] : null), "previous"), "html", null, true);
                echo "\">&larr; Newer</a>
    </li>
    ";
            }
            // line 34
            echo "    ";
            if (($this->getAttribute((isset($context["pager"]) ? $context["pager"] : null), "current") != $this->getAttribute((isset($context["pager"]) ? $context["pager"] : null), "last"))) {
                // line 35
                echo "    <li class=\"next\">
        <a href=\"?page=";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pager"]) ? $context["pager"] : null), "next"), "html", null, true);
                echo "\">Older &rarr;</a>
    </li>
    ";
            }
            // line 39
            echo "</ul>
";
        }
    }

    public function getTemplateName()
    {
        return "commits_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 39,  111 => 36,  108 => 35,  105 => 34,  99 => 31,  96 => 30,  94 => 29,  91 => 28,  89 => 27,  86 => 26,  82 => 24,  73 => 20,  59 => 16,  55 => 15,  49 => 14,  44 => 12,  41 => 11,  30 => 6,  25 => 3,  21 => 2,  19 => 1,  48 => 11,  46 => 10,  43 => 9,  40 => 8,  37 => 10,  31 => 5,  26 => 3,);
    }
}
