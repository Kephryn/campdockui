<?php

/* breadcrumb.twig */
class __TwigTemplate_c8bf0cbf800c9190711a5c0f7ab6b77f66f246f4be7c74f6e6b2144fc007f8b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'extra' => array($this, 'block_extra'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"breadcrumb\">
    <li><a href=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tree", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["repo"]) ? $context["repo"] : null), "html", null, true);
        echo "</a></li>
    ";
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 4
            echo "        <span class=\"divider\">/</span>
        <li";
            // line 5
            if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                echo " class=\"active\"";
            }
            echo ">";
            if ((!$this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last"))) {
                echo "<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tree", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (((isset($context["branch"]) ? $context["branch"] : null) . "/") . $this->getAttribute((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "path")))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "dir"), "html", null, true);
                echo "</a>";
            }
            if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "dir"), "html", null, true);
            }
            echo "</li>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "
    ";
        // line 8
        $this->displayBlock('extra', $context, $blocks);
        // line 9
        echo "</ul>
";
    }

    // line 8
    public function block_extra($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "breadcrumb.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 8,  83 => 9,  49 => 5,  23 => 2,  20 => 1,  38 => 4,  30 => 3,  72 => 19,  69 => 18,  54 => 15,  51 => 14,  33 => 10,  22 => 2,  19 => 1,  106 => 27,  101 => 30,  96 => 28,  89 => 24,  87 => 23,  84 => 22,  81 => 8,  78 => 7,  76 => 19,  58 => 16,  52 => 11,  41 => 9,  35 => 5,  32 => 4,  29 => 3,  220 => 15,  213 => 13,  207 => 12,  203 => 10,  200 => 9,  153 => 69,  147 => 66,  142 => 64,  138 => 62,  135 => 61,  131 => 59,  126 => 56,  114 => 53,  110 => 52,  105 => 51,  102 => 49,  99 => 29,  97 => 46,  94 => 27,  91 => 44,  86 => 43,  79 => 38,  73 => 18,  67 => 15,  65 => 33,  61 => 31,  59 => 30,  48 => 13,  46 => 4,  43 => 10,  40 => 8,  37 => 11,  31 => 5,  26 => 3,);
    }
}
