<?php

/* rss.twig */
class __TwigTemplate_0050629f0df7f5144cc92c024fca77c0d4a33d1bc539b05c22dfeafb443ee615 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
<rss version=\"2.0\">
    <channel>
        <title>Latest commits in ";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["repo"]) ? $context["repo"] : null), "html", null, true);
        echo ":";
        echo twig_escape_filter($this->env, (isset($context["branch"]) ? $context["branch"] : null), "html", null, true);
        echo "</title>
        <description>RSS provided by GitList</description>
        <link>";
        // line 6
        echo $this->env->getExtension('routing')->getUrl("homepage");
        echo "</link>

        ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["commits"]) ? $context["commits"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["commit"]) {
            // line 9
            echo "        <item>
            <title>";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "message"), "html", null, true);
            echo "</title>
            <description>";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "author"), "name"), "html", null, true);
            echo " authored ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "shortHash"), "html", null, true);
            echo " in ";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('format_date')->getCallable(), array($this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "date"))), "html", null, true);
            echo "</description>
            <link>";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("commit", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commit" => $this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "hash"))), "html", null, true);
            echo "</link>
            <pubDate>";
            // line 13
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "date"), "r"), "html", null, true);
            echo "</pubDate>
        </item>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['commit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "    </channel>
</rss>
";
    }

    public function getTemplateName()
    {
        return "rss.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 16,  59 => 13,  55 => 12,  47 => 11,  43 => 10,  40 => 9,  36 => 8,  31 => 6,  24 => 4,  19 => 1,);
    }
}
