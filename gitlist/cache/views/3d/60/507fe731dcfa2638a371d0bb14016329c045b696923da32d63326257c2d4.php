<?php

/* menu.twig */
class __TwigTemplate_3d60507fe731dcfa2638a371d0bb14016329c045b696923da32d63326257c2d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"nav nav-tabs\">
    <li";
        // line 2
        if (((isset($context["page"]) ? $context["page"] : null) == "files")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("branch", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
        echo "\">Files</a></li>
    <li";
        // line 3
        if (twig_in_filter((isset($context["page"]) ? $context["page"] : null), array(0 => "commits", 1 => "searchcommits"))) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commits", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
        echo "\">Commits</a></li>
    <li";
        // line 4
        if (((isset($context["page"]) ? $context["page"] : null) == "stats")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("stats", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
        echo "\">Stats</a></li>
  \t<li";
        // line 5
        if (((isset($context["page"]) ? $context["page"] : null) == "network")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("network", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
        echo "\">Network</a></li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 4,  30 => 3,  72 => 19,  69 => 18,  54 => 15,  51 => 14,  33 => 10,  22 => 2,  19 => 1,  106 => 27,  101 => 30,  96 => 28,  89 => 24,  87 => 23,  84 => 22,  81 => 21,  78 => 20,  76 => 19,  58 => 16,  52 => 11,  41 => 9,  35 => 5,  32 => 4,  29 => 3,  220 => 15,  213 => 13,  207 => 12,  203 => 10,  200 => 9,  153 => 69,  147 => 66,  142 => 64,  138 => 62,  135 => 61,  131 => 59,  126 => 56,  114 => 53,  110 => 52,  105 => 51,  102 => 49,  99 => 29,  97 => 46,  94 => 27,  91 => 44,  86 => 43,  79 => 38,  73 => 18,  67 => 15,  65 => 33,  61 => 31,  59 => 30,  48 => 13,  46 => 5,  43 => 10,  40 => 8,  37 => 11,  31 => 5,  26 => 3,);
    }
}
