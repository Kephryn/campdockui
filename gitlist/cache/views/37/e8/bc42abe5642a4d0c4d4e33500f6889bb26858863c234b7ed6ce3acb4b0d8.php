<?php

/* tree.twig */
class __TwigTemplate_37e8bc42abe5642a4d0c4d4e33500f6889bb26858863c234b7ed6ce3acb4b0d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout_page.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout_page.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["page"] = "files";
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "GitList";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->env->loadTemplate("tree.twig", "1084093090")->display(array_merge($context, array("breadcrumbs" => (isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null))));
        // line 19
        echo "
    ";
        // line 20
        if ((!twig_test_empty((isset($context["files"]) ? $context["files"] : null)))) {
            // line 21
            echo "    <table class=\"tree\">
        <thead>
            <tr>
                <th width=\"80%\">name</th>
                <th width=\"10%\">mode</th>
                <th width=\"10%\">size</th>
            </tr>
        </thead>
        <tbody>
            ";
            // line 30
            if ((!(null === (isset($context["parent"]) ? $context["parent"] : null)))) {
                // line 31
                echo "            <tr>
                <td><i class=\"icon-spaced\"></i>
                    ";
                // line 33
                if ((!(isset($context["parent"]) ? $context["parent"] : null))) {
                    // line 34
                    echo "                        <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("branch", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
                    echo "\">..</a>
                    ";
                } else {
                    // line 36
                    echo "                        <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tree", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (((isset($context["branch"]) ? $context["branch"] : null) . "/") . (isset($context["parent"]) ? $context["parent"] : null)))), "html", null, true);
                    echo "\">..</a>
                    ";
                }
                // line 38
                echo "                </td>
                <td></td>
                <td></td>
            </tr>
            ";
            }
            // line 43
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["files"]) ? $context["files"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
                // line 44
                echo "            <tr>
                <td><i class=\"";
                // line 45
                echo (((($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "type") == "folder") || ($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "type") == "symlink"))) ? ("icon-folder-open") : ("icon-file"));
                echo " icon-spaced\"></i> <a href=\"";
                // line 46
                if ((($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "type") == "folder") || ($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "type") == "symlink"))) {
                    // line 47
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tree", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => ((((isset($context["branch"]) ? $context["branch"] : null) . "/") . (isset($context["path"]) ? $context["path"] : null)) . ((($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "type") == "symlink")) ? ($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "path")) : ($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "name")))))), "html", null, true);
                } else {
                    // line 49
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blob", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => ((((isset($context["branch"]) ? $context["branch"] : null) . "/") . (isset($context["path"]) ? $context["path"] : null)) . ((($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "type") == "symlink")) ? ($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "path")) : ($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "name")))))), "html", null, true);
                }
                // line 51
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["file"]) ? $context["file"] : null), "name"), "html", null, true);
                echo "</a></td>
                <td>";
                // line 52
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["file"]) ? $context["file"] : null), "mode"), "html", null, true);
                echo "</td>
                <td>";
                // line 53
                if ($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "size")) {
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute((isset($context["file"]) ? $context["file"] : null), "size") / 1024)), "html", null, true);
                    echo " kb";
                }
                echo "</td>
            </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "        </tbody>
    </table>
    ";
        } else {
            // line 59
            echo "        <p>This repository is empty.</p>
    ";
        }
        // line 61
        echo "    ";
        if ((array_key_exists("readme", $context) && (!twig_test_empty((isset($context["readme"]) ? $context["readme"] : null))))) {
            // line 62
            echo "        <div class=\"readme-view\">
            <div class=\"md-header\">
                <div class=\"meta\">";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["readme"]) ? $context["readme"] : null), "filename"), "html", null, true);
            echo "</div>
            </div>
            <div id=\"md-content\">";
            // line 66
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["readme"]) ? $context["readme"] : null), "content"), "html", null, true);
            echo "</div>
        </div>
    ";
        }
        // line 69
        echo "
    <hr />
";
    }

    public function getTemplateName()
    {
        return "tree.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 69,  147 => 66,  142 => 64,  138 => 62,  135 => 61,  131 => 59,  126 => 56,  114 => 53,  110 => 52,  105 => 51,  102 => 49,  99 => 47,  97 => 46,  94 => 45,  91 => 44,  86 => 43,  79 => 38,  73 => 36,  67 => 34,  65 => 33,  61 => 31,  59 => 30,  48 => 21,  46 => 20,  43 => 19,  40 => 8,  37 => 7,  31 => 5,  26 => 3,);
    }
}


/* tree.twig */
class __TwigTemplate_37e8bc42abe5642a4d0c4d4e33500f6889bb26858863c234b7ed6ce3acb4b0d8_1084093090 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("breadcrumb.twig");

        $this->blocks = array(
            'extra' => array($this, 'block_extra'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "breadcrumb.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_extra($context, array $blocks = array())
    {
        // line 10
        echo "            <div class=\"pull-right\">
                <div class=\"btn-group download-buttons\">
                    <a href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("archive", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["branch"]) ? $context["branch"] : null), "format" => "zip")), "html", null, true);
        echo "\" class=\"btn btn-mini\" title=\"Download '";
        echo twig_escape_filter($this->env, (isset($context["branch"]) ? $context["branch"] : null), "html", null, true);
        echo "' as a ZIP archive\">ZIP</a>
                    <a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("archive", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["branch"]) ? $context["branch"] : null), "format" => "tar")), "html", null, true);
        echo "\" class=\"btn btn-mini\" title=\"Download '";
        echo twig_escape_filter($this->env, (isset($context["branch"]) ? $context["branch"] : null), "html", null, true);
        echo "' as a TAR archive\">TAR</a>
                </div>
                <a href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("rss", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => (isset($context["branch"]) ? $context["branch"] : null))), "html", null, true);
        echo "\" class=\"rss-icon\"><i class=\"rss\"></i></a>
            </div>
        ";
    }

    public function getTemplateName()
    {
        return "tree.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  220 => 15,  213 => 13,  207 => 12,  203 => 10,  200 => 9,  153 => 69,  147 => 66,  142 => 64,  138 => 62,  135 => 61,  131 => 59,  126 => 56,  114 => 53,  110 => 52,  105 => 51,  102 => 49,  99 => 47,  97 => 46,  94 => 45,  91 => 44,  86 => 43,  79 => 38,  73 => 36,  67 => 34,  65 => 33,  61 => 31,  59 => 30,  48 => 21,  46 => 20,  43 => 19,  40 => 8,  37 => 7,  31 => 5,  26 => 3,);
    }
}
