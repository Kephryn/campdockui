<?php

/* commit.twig */
class __TwigTemplate_6f00595f481ab3db1a2bd5212ac6d7731dc5e8e1e1ecae604594d83f7b3cd950 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout_page.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout_page.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["page"] = "commits";
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "GitList";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->env->loadTemplate("breadcrumb.twig")->display(array_merge($context, array("breadcrumbs" => array(0 => array("dir" => ("Commit " . $this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "hash")), "path" => "")))));
        // line 9
        echo "
    <div class=\"commit-view\">
        <div class=\"commit-header\">
            <span class=\"pull-right\"><a class=\"btn btn-small\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("branch", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "branch" => $this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "hash"))), "html", null, true);
        echo "\" title=\"Browse code at this point in history\"><i class=\"icon-list-alt\"></i> Browse code</a></span>
            <h4>";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "message"), "html", null, true);
        echo "</h4>
        </div>
        <div class=\"commit-body\">
            ";
        // line 16
        if ((!twig_test_empty($this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "body")))) {
            // line 17
            echo "            <p>";
            echo nl2br(twig_escape_filter($this->env, $this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "body"), "html", null, true));
            echo "</p>
            ";
        }
        // line 19
        echo "            <img src=\"https://gravatar.com/avatar/";
        echo twig_escape_filter($this->env, md5(twig_lower_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "author"), "email"))), "html", null, true);
        echo "?s=32\" class=\"pull-left space-right\" />
            <span><a href=\"mailto:";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "author"), "email"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "author"), "name"), "html", null, true);
        echo "</a> authored on ";
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('format_date')->getCallable(), array($this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "date"))), "html", null, true);
        echo "<br />Showing ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "changedFiles"), "html", null, true);
        echo " changed files</span>
        </div>
    </div>

    <ul class=\"commit-list\">
        ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "diffs"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["diff"]) {
            // line 26
            echo "            <li><i class=\"icon-file\"></i> <a href=\"#";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["diff"]) ? $context["diff"] : null), "file"), "html", null, true);
            echo "</a> <span class=\"meta pull-right\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["diff"]) ? $context["diff"] : null), "index"), "html", null, true);
            echo "</span></li>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['diff'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "    </ul>

    ";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "diffs"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["diff"]) {
            // line 31
            echo "    <div class=\"source-view\">
        <div class=\"source-header\">
            <div class=\"meta\"><a name=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["diff"]) ? $context["diff"] : null), "file"), "html", null, true);
            echo "</div>

            <div class=\"btn-group pull-right\">
                <a href=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commits", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (($this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "hash") . "/") . $this->getAttribute((isset($context["diff"]) ? $context["diff"] : null), "file")))), "html", null, true);
            echo "\" class=\"btn btn-small\"><i class=\"icon-list-alt\"></i> History</a>
                <a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blob", array("repo" => (isset($context["repo"]) ? $context["repo"] : null), "commitishPath" => (($this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "hash") . "/") . $this->getAttribute((isset($context["diff"]) ? $context["diff"] : null), "file")))), "html", null, true);
            echo "\" class=\"btn btn-small\"><i class=\"icon-file\"></i> View file @ ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commit"]) ? $context["commit"] : null), "shortHash"), "html", null, true);
            echo "</a>
            </div>
        </div>

        <div class=\"source-diff\">
        <table>
        ";
            // line 43
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["diff"]) ? $context["diff"] : null), "getLines"));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["line"]) {
                // line 44
                echo "            <tr>
                <td class=\"lineNo\">
                    ";
                // line 46
                if (($this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getType") != "chunk")) {
                    // line 47
                    echo "                        <a name=\"L";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                    echo "R";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getNumOld"), "html", null, true);
                    echo "\"></a>
                        <a href=\"#L";
                    // line 48
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                    echo "L";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getNumOld"), "html", null, true);
                    echo "\">
                    ";
                }
                // line 50
                echo "                    ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getNumOld"), "html", null, true);
                echo "
                    ";
                // line 51
                if (($this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getType") != "chunk")) {
                    // line 52
                    echo "                        </a>
                    ";
                }
                // line 54
                echo "                </td>
                <td class=\"lineNo\">
                    ";
                // line 56
                if (($this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getType") != "chunk")) {
                    // line 57
                    echo "                        <a name=\"L";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                    echo "L";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getNumNew"), "html", null, true);
                    echo "\"></a>
                        <a href=\"#L";
                    // line 58
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                    echo "L";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getNumNew"), "html", null, true);
                    echo "\">
                    ";
                }
                // line 60
                echo "                    ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getNumNew"), "html", null, true);
                echo "
                    ";
                // line 61
                if (($this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getType") != "chunk")) {
                    // line 62
                    echo "                        </a>
                    ";
                }
                // line 64
                echo "                </td>
                <td style=\"width: 100%\">
                    <pre";
                // line 66
                if ($this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getType")) {
                    echo " class=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getType"), "html", null, true);
                    echo "\"";
                }
                echo ">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["line"]) ? $context["line"] : null), "getLine"), "html", null, true);
                echo "</pre>
                </td>
            </tr>
        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['line'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "        </table>
        </div>
    </div>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['diff'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "
    <hr />
";
    }

    public function getTemplateName()
    {
        return "commit.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  295 => 74,  278 => 70,  254 => 66,  250 => 64,  246 => 62,  244 => 61,  239 => 60,  232 => 58,  225 => 57,  223 => 56,  219 => 54,  215 => 52,  213 => 51,  208 => 50,  201 => 48,  194 => 47,  192 => 46,  188 => 44,  171 => 43,  160 => 37,  156 => 36,  148 => 33,  144 => 31,  127 => 30,  123 => 28,  102 => 26,  85 => 25,  71 => 20,  66 => 19,  60 => 17,  58 => 16,  52 => 13,  48 => 12,  43 => 9,  40 => 8,  37 => 7,  31 => 5,  26 => 3,);
    }
}
