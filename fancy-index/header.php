<?php
require_once '../template/includes.php';
?>
	<!-- Bootstrap -->
	<link href="/camp-dock/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="/camp-dock/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="/camp-dock/gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- iCheck -->
	<link href="/camp-dock/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	<!-- bootstrap-progressbar -->
	<link href="/camp-dock/gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
	<!-- JQVMap -->
	<link href="/camp-dock/gentelella/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
	<!-- bootstrap-daterangepicker -->
	<link href="/camp-dock/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="/camp-dock/gentelella/build/css/custom.min.css" rel="stylesheet">

	<script>
	// var es = new EventSource('/stream');

	// // es.onmessage = function (event) {
	// //   console.log(event);
	// // };

	// es.addEventListener('system-statistic', function (event) {
	// 	console.log(event);
	// });
	document.body.className += 'nav-md';
	</script>

	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col menu_fixed">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="/" class="site_title">
							<img height="32" width="32" style="stroke: white" src="/camp-dock/fancy-index/icons/icon_features_camping.svg">
							<span>Camp Dock </span>
						</a>
					</div>

					<div class="clearfix"></div>

					<!-- menu profile quick info -->
					<!-- <div class="profile">
						<div class="profile_pic">
							<img src="/camp-dock/fancy-index/icons/icon_features_camping.svg" alt="..." class="img-circle profile_img">
							<i class="fa fa-fire fa-4" aria-hidden="true"></i>
						</div>
						<div class="profile_info">
							<span>Welcome,</span>
							<h2>User</h2>
						</div>
					</div> -->
					<!-- /menu profile quick info -->

					<br />

					<!-- sidebar menu -->
					<?php require __DIR__.'/../template/menu.php';?>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Settings">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="FullScreen">
							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Lock">
							<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Logout">
							<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
						</a>
					</div>
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- page content -->
			<div class="right_col" role="main" style="min-height: 100%">
				<!-- top tiles -->
				<div class="row tile_count">
					<div class="col-md-4 col-sm-12 col-xs-12 tile_stats_count">
						<span class="count_top"><i class="fa fa-clock-o"></i> <?php echo exec('uname -o')?></span>
						<div class="count"><?php echo semver(exec('uname -r'))?></div>
						<!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> </span> -->
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 tile_stats_count">
						<span class="count_top"><i class="fa fa-user"></i> Web Server</span>
						<div class="count"><?php echo semver($_SERVER['SERVER_SOFTWARE'])?></div>
						<!-- <span class="count_bottom"><i class="green">4% </i>  </span> -->
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 tile_stats_count">
						<span class="count_top"><i class="fa fa-clock-o"></i> PHP</span>
						<div class="count"><?php echo semver(exec('php -v'))?></div>
						<!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> </span> -->
					</div>
				</div>
				<!-- /top tiles -->

				<div class="row" id="main-content">

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Directory Index <small> The listing of files on your local server</small>
								</h2>

								<ul class="nav navbar-right panel_toolbox">
									<li>
										<a class="collapse-link">
											<i class="fa fa-chevron-up">
											</i>
										</a>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
											<i class="fa fa-wrench">
											</i>
										</a>
										<ul class="dropdown-menu" role="menu">
											<li>
												<a href="#">Settings 1</a>
											</li>
											<li>
												<a href="#">Settings 2</a>
											</li>
										</ul>
									</li>
									<li>
										<a class="close-link">
											<i class="fa fa-close">
											</i>
										</a>
									</li>
								</ul>
								<div class="clearfix">
								</div>

							</div>
							<div class="x_content">
								<div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
									<?php echo breadcrumbs()?>

