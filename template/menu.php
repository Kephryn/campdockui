<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	<div class="menu_section">
		<!-- <h3>General</h3> -->
		<ul class="nav side-menu">
			<li class="active">
				<a>
					<i class="fa fa-server"></i> Server <span class="fa fa-chevron-down"></span>
				</a>
				<ul class="nav child_menu" style="display: block">
					<li><a href="/camp-dock/dashboard.html"><i class="fa fa-tachometer"></i> Dashboard</a></li>
					<li><a href="/camp-dock/web-server.html"><i class="fa fa-globe"></i>Web Server</a></li>
					<li class="active"><a href="http://camp-dock.dock0/template/frame.php?frame=/template/info.php"><i class="fa fa-info-circle"></i>PHP Info</a></li>
					<li><a href="index2.html"><i class="fa fa-database"></i>Database Server</a></li>
					<li><a href="/camp-dock/apache/cgi"><i class="fa fa-gear"></i>CGI Handlers</a></li>
				</ul>
			</li>
			<li><a><i class="fa fa-database"></i> DB Management <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="http://camp-dock.dock0/template/frame.php?frame=/database/adminer/index.php">Adminer</a></li>
					<li><a href="http://camp-dock.dock0/template/frame.php?frame=/database/php-redis-admin/index.php">PHP Redis Admin</a></li>
					<li><a href="http://camp-dock.dock0/template/frame.php?frame=/database/php-my-admin/index.php">PHP My Admin</a></li>
				</ul>
			</li>
			<li><a><i class="fa fa-git"></i> Git <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="http://camp-dock.dock0/template/frame.php?frame=/gitlist/index.php">GitList</a></li>
				</ul>
			</li>
			<li><a><i class="fa fa-edit"></i> Logs <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="dashboard.html">Apache Logs</a></li>
				</ul>
			</li>

		</ul>
	</div>
</div>