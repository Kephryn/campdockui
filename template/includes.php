<?php
function semver($input) {
	preg_match('/\d\.\d\.\d/', $input, $matches);
	return $matches[0];
}

function breadcrumbs() {
	$crumbs = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
	$link = '/';
	$links = "<li><a href='/'>root</a></li>";

	foreach($crumbs as $crumb) {
		$link .= $crumb . '/';
		$links .= sprintf("<li><a href='%s'>%s</a></li>", $link, $crumb);
	}

	return "<ol class='breadcrumb'>$links</ol>";
}