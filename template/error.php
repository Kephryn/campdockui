<?php
$code = isset($_GET['code']) ? $_GET['code'] : false;
switch($code) {
	case 400:
		$title = 'Bad Request';
		$message = 'The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications.';
		break;
	case 401:
		$title = 'Unauthorized';
		$message = 'The request requires user authentication. The response MUST include a WWW-Authenticate header field (section 14.47) containing a challenge applicable to the requested resource. The client MAY repeat the request with a suitable Authorization header field (section 14.8). If the request already included Authorization credentials, then the 401 response indicates that authorization has been refused for those credentials. If the 401 response contains the same challenge as the prior response, and the user agent has already attempted authentication at least once, then the user SHOULD be presented the entity that was given in the response, since that entity might include relevant diagnostic information. HTTP access authentication is explained in "HTTP Authentication: Basic and Digest Access Authentication"';
		break;
	case 403:
		$title = 'Forbidden';
		$message = 'The server understood the request, but is refusing to fulfill it. Authorization will not help and the request SHOULD NOT be repeated. If the request method was not HEAD and the server wishes to make public why the request has not been fulfilled, it SHOULD describe the reason for the refusal in the entity. If the server does not wish to make this information available to the client, the status code 404 (Not Found) can be used instead.';
		break;
	case 404:
		$title = 'Not Found';
		$message = 'The server has not found anything matching the Request-URI. No indication is given of whether the condition is temporary or permanent. The 410 (Gone) status code SHOULD be used if the server knows, through some internally configurable mechanism, that an old resource is permanently unavailable and has no forwarding address. This status code is commonly used when the server does not wish to reveal exactly why the request has been refused, or when no other response is applicable.';
		break;
	case 405:
		$title = 'Method Not Allowed';
		$message = 'The method specified in the Request-Line is not allowed for the resource identified by the Request-URI. The response MUST include an Allow header containing a list of valid methods for the requested resource.';
		break;
	case 408:
		$title = 'Request Timeout';
		$message = 'The client did not produce a request within the time that the server was prepared to wait. The client MAY repeat the request without modifications at any later time.';
		break;
	case 410:
		$title = 'Gone';
		$message = 'The requested resource is no longer available at the server and no forwarding address is known. This condition is expected to be considered permanent. Clients with link editing capabilities SHOULD delete references to the Request-URI after user approval. If the server does not know, or has no facility to determine, whether or not the condition is permanent, the status code 404 (Not Found) SHOULD be used instead. This response is cacheable unless indicated otherwise.

			The 410 response is primarily intended to assist the task of web maintenance by notifying the recipient that the resource is intentionally unavailable and that the server owners desire that remote links to that resource be removed. Such an event is common for limited-time, promotional services and for resources belonging to individuals no longer working at the server\'s site. It is not necessary to mark all permanently unavailable resources as "gone" or to keep the mark for any length of time -- that is left to the discretion of the server owner.';
		break;
	case 411:
		$title = 'Length Required';
		$message = 'The server refuses to accept the request without a defined Content- Length. The client MAY repeat the request if it adds a valid Content-Length header field containing the length of the message-body in the request message.';
		break;
	case 412:
		$title = 'Precondition Failed';
		$message = 'The precondition given in one or more of the request-header fields evaluated to false when it was tested on the server. This response code allows the client to place preconditions on the current resource metainformation (header field data) and thus prevent the requested method from being applied to a resource other than the one intended.';
		break;
	case 413:
		$title = 'Request Entity Too Large';
		$message = 'The server is refusing to process a request because the request entity is larger than the server is willing or able to process. The server MAY close the connection to prevent the client from continuing the request.

			If the condition is temporary, the server SHOULD include a Retry- After header field to indicate that it is temporary and after what time the client MAY try again.';
		break;
	case 414:
		$title = 'Request-URI Too Long';
		$message = 'The server is refusing to service the request because the Request-URI is longer than the server is willing to interpret. This rare condition is only likely to occur when a client has improperly converted a POST request to a GET request with long query information, when the client has descended into a URI "black hole" of redirection (e.g., a redirected URI prefix that points to a suffix of itself), or when the server is under attack by a client attempting to exploit security holes present in some servers using fixed-length buffers for reading or manipulating the Request-URI.';
		break;
	case 415:
		$title = 'Unsupported Media Type';
		$message = 'The server is refusing to service the request because the entity of the request is in a format not supported by the requested resource for the requested method.';
		break;
	case 500:
		$title = 'Internal Server Error';
		$message = 'The server encountered an unexpected condition which prevented it from fulfilling the request.';
		break;
	case 501:
		$title = 'Not Implemented';
		$message = 'The server does not support the functionality required to fulfill the request. This is the appropriate response when the server does not recognize the request method and is not capable of supporting it for any resource.';
		break;
	case 502:
		$title = 'Bad Gateway';
		$message = 'The server, while acting as a gateway or proxy, received an invalid response from the upstream server it accessed in attempting to fulfill the request.';
		break;
	case 503:
		$title = 'Service Unavailable';
		$message = 'The server is currently unable to handle the request due to a temporary overloading or maintenance of the server. The implication is that this is a temporary condition which will be alleviated after some delay. If known, the length of the delay MAY be indicated in a Retry-After header. If no Retry-After is given, the client SHOULD handle the response as it would for a 500 response.

      		Note: The existence of the 503 status code does not imply that a server must use it when becoming overloaded. Some servers may wish to simply refuse the connection.';
		break;
	case 506:
		$title = 'Variant Also Negotiates';
		$message = 'The server has an internal configuration error: the chosen variant resource is configured to engage in transparent content negotiation itself, and is therefore not a proper end point in the negotiation process.';
		break;
	default:
		$title = 'Unknown Error';
		$message = 'Error code: '.$code;
		break;

}

$__content = "<h1>[$code] $title</h1><p>" .nl2br($message). "</p>";

// echo $__content;

require_once 'frame.php';