fs = require 'fs'
cgi = require './cgi'
coffee = require 'coffee-script'

server = cgi.createServer (request, response) ->
	response.writeHead 200, 'Content-Type': 'text/plain'

	try
		fs.readFile process.env.PATH_TRANSLATED, 'utf8', (err, data) ->
			response.end err if err
			response.end coffee.compile data
	catch err
		response.end JSON.stringify err

server.listen()